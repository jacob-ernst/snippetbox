include .env
PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
GOPATH="$(GOBASE)/vendor:$(GOBASE):/Users/jacobernst/go"
GOBIN=$(GOBASE)/bin
GOFILES=$(wildcard cmd/web/*.go)

# PID file will keep the process id of the server
PID=/tmp/.$(PROJECTNAME).pid

#.DELETE_ON_ERROR:

.envrc: .env
	cat .env | sed -e 's/^/export /' > .envrc

.env:
	cat .env.example | sed -e 's/^# .*//' > .env
	sed -i "" '/^[[:space:]]*$$/d' .env

start-server: stop-server
	@echo "  >  $(PROJECTNAME) is available at $(ADDR)"
	@echo $(DB_DSN)
	@-$(GOBIN)/$(PROJECTNAME) -dsn=$(DB_DSN) 2>&1 & echo $$! > $(PID)
	@cat $(PID) | sed "/^/s/^/  \>  PID: /"

stop-server:
	@-touch $(PID)
	@-kill `cat $(PID)` 2> /dev/null || true
	@-rm $(PID)

go-build:
	@echo "  >  Building binary..."
	@GOPATH=$(GOPATH) GOBIN=$(GOBIN) go build -o $(GOBIN)/$(PROJECTNAME) $(GOFILES)

.PHONY: clean
clean:
	rm -f .envrc
